#pragma once

#include "AreaColor.hpp"

#include <array>
#include <jngl.hpp>
#include <optional>

class BorderArea;
class EditorObject;
class Game;

class Level {
public:
	explicit Level(Game&);
	~Level();

	Level(const Level&) = delete;
	Level(Level&&) = delete;
	Level& operator=(const Level&) = delete;
	Level& operator=(Level&&) = delete;

	BorderArea* getBorderAreaAt(jngl::Vec2 position);

	void triangulateBorder();

	/// Gibt zurück, ob die Maus über mindestens einem EditorObject ist
	bool mouseOverEditorObject() const;

	void stepForEditor(std::optional<std::array<jngl::Vec2, 2>> selectInRectangle);

	void save() const;

	void addBorderArea(jngl::Vec2 position);

	void drawForEditor() const;

private:
	std::vector<std::array<jngl::Vec2, 3>>
	triangulateBorder(std::vector<jngl::Vec2> vertices, std::vector<std::vector<jngl::Vec2>> holes,
	                  AreaColor);

	Game& game;

	std::vector<BorderArea> borderAreas;

	/// Alle EditorObjects bis auf die VertexGrabber
	std::vector<std::unique_ptr<EditorObject>> editorObjects;

	/// Sind im LevelEditor mehrere Vertexes markiert? TODO: Sollte nicht Teil von Level sein.
	bool multipleSelection = false;
};
