#include "LevelEditor.hpp"

#include "gui/Button.hpp"
#include "BorderArea.hpp"
#include "engine/helper.hpp"
#include "Game.hpp"
#include "Level.hpp"

#include <array>
#include <fmt/format.h>
#include <fmt/ostream.h>
#include <jngl/input.hpp>
#include <random>

LevelEditor::LevelEditor(std::shared_ptr<Game> game, Level& level)
: game(std::move(game)), previousMouseVisibility(jngl::isMouseVisible()), level(level) {
	jngl::setMouseVisible(true);
	container.addWidget<Button>("Neues Gebiet", jngl::Vec2(-235, -170),
	                            [this]() { mouseType = MouseType::ADD_BORDER_AREA; });
	// AddWidget(std::make_shared<Button>("ScriptObject hinzufügen", jngl::Vec2(-425, -550),
	//                                    [this]() { mouseType = MouseType::ADD_SCRIPT_OBJECT; }));
	// AddWidget(std::make_shared<Button>("SavePoint hinzufügen", jngl::Vec2(425, -550),
	//                                    [this]() { mouseType = MouseType::ADD_SAVE_POINT; }));
	// AddWidget(std::make_shared<Button>("2 Teleporter hinzufügen", jngl::Vec2(850, -550),
	//                                    [this]() { mouseType = MouseType::ADD_TELEPORTER; }));
	// level.removeSpawnedFromEditorObjects();
}

LevelEditor::~LevelEditor() {
	jngl::setMouseVisible(previousMouseVisibility);
}

void LevelEditor::step() {
	switch (mouseType) {
	case MouseType::SELECT: {
		container.step();
		if (jngl::mousePressed(jngl::mouse::Left) && !level.mouseOverEditorObject()) {
			jngl::setMousePressed(jngl::mouse::Left, false);
			selectionRectangle = { game->getAbsoluteMousePos(), game->getAbsoluteMousePos() };
		}
		if (selectionRectangle) {
			(*selectionRectangle)[1] = game->getAbsoluteMousePos();
		}
		std::optional<std::array<jngl::Vec2, 2>> selectInRectangle;
		if (selectionRectangle && !jngl::mouseDown(jngl::mouse::Left)) {
			selectInRectangle = selectionRectangle;
			selectionRectangle = std::nullopt;

			// Für die Kollisionserkennung muss das Rechteck richtig geordnet sein:
			if ((*selectInRectangle)[0].x > (*selectInRectangle)[1].x) {
				std::swap((*selectInRectangle)[0].x, (*selectInRectangle)[1].x);
			}
			if ((*selectInRectangle)[0].y > (*selectInRectangle)[1].y) {
				std::swap((*selectInRectangle)[0].y, (*selectInRectangle)[1].y);
			}
		}
		if ((jngl::keyDown(jngl::key::ControlL) or jngl::keyDown(jngl::key::ControlR)) and
		    jngl::keyPressed('a')) { // Strg+A == alles markieren
			// Hack: Mega großes Rechteck benutzen:
			selectInRectangle = { jngl::Vec2(-1e99, -1e99), jngl::Vec2(1e99, 1e99) };
		}
		level.stepForEditor(selectInRectangle);
		jngl::setTitle(
		    fmt::format("Level Editor | Mouse: {}", roundToGrid(game->getAbsoluteMousePos())));
		break;
	}
	case MouseType::ADD_BORDER_AREA:
		if (jngl::mousePressed(jngl::mouse::Left)) {
			const auto newPos = roundToGrid(game->getAbsoluteMousePos());
			if (const auto borderArea = level.getBorderAreaAt(newPos)) {
				borderArea->addHole(newPos);
			} else {
				level.addBorderArea(newPos);
				game->triangulateBorder();
			}
			mouseType = MouseType::SELECT;
		}
		break;
	}

	const double CAMERA_MOVESPEED = 200 / game->getCameraZoom();
	jngl::Vec2 cameraSpeed;
	if (jngl::keyDown(jngl::key::Left)) {
		cameraSpeed += jngl::Vec2(-CAMERA_MOVESPEED, 0);
	}
	if (jngl::keyDown(jngl::key::Right)) {
		cameraSpeed += jngl::Vec2(CAMERA_MOVESPEED, 0);
	}
	if (jngl::keyDown(jngl::key::Up)) {
		cameraSpeed += jngl::Vec2(0, -CAMERA_MOVESPEED);
	}
	if (jngl::keyDown(jngl::key::Down)) {
		cameraSpeed += jngl::Vec2(0, CAMERA_MOVESPEED);
	}
	game->setCameraPosition(game->getCameraPosition() + cameraSpeed, 0, 0);
	if (grabMousePos) {
		game->setCameraPositionImmediately(*grabMousePos -
		                                   jngl::getMousePos() / game->getCameraZoom());
		if (!jngl::mouseDown(jngl::mouse::Right)) {
			grabMousePos = std::nullopt;
		}
	} else if (jngl::mousePressed(jngl::mouse::Right)) {
		grabMousePos = game->getAbsoluteMousePos();
	}
	game->stepCamera(true);

	if (jngl::keyPressed(jngl::key::F3)) {
		level.save();
		// level.spawnFromEditorObjects();
		jngl::setWork(game);
	}
}

void LevelEditor::draw() const {
	jngl::pushMatrix();
	game->applyCamera();
	jngl::setColor(0xbbbbbb_rgb);
	jngl::setAlpha(255);

	jngl::Vec2 start(-jngl::getScreenSize() / 2.);
	while (start.y < jngl::getScreenSize().y / 2 + GRID_SIZE / 2.) {
		jngl::drawLine(start, start + jngl::Vec2(jngl::getScreenSize().x, 0));
		start.y += GRID_SIZE;
	}

	start = -jngl::getScreenSize() / 2.;
	while (start.x < jngl::getScreenSize().x / 2 + GRID_SIZE / 2.) {
		jngl::drawLine(start, start + jngl::Vec2(0, jngl::getScreenSize().y));
		start.x += GRID_SIZE;
	}

	jngl::popMatrix();

	game->draw();

	jngl::pushMatrix();
	game->applyCamera();
	level.drawForEditor();
	if (selectionRectangle) {
		jngl::setColor(34, 255, 34, 100);
		jngl::drawRect((*selectionRectangle)[0],
		               ((*selectionRectangle)[1] - (*selectionRectangle)[0]));
	}
	jngl::popMatrix();
	container.draw();
}

void LevelEditor::onQuitEvent() {
	level.save();
}
