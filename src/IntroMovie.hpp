#pragma once

#include "spine/AnimationState.h"

#include <jngl/work.hpp>
#include <string>

struct spSkeleton;
struct spAnimationState;
struct spEvent;

namespace spine {
class SkeletonDrawable;
}

class IntroMovie : public jngl::Work {
public:
	IntroMovie();
	~IntroMovie();
	void step() override;
	void draw() const override;

private:
	std::unique_ptr<spine::SkeletonDrawable> skeleton;
    spSkeletonData* skeletonData = nullptr;

    // spSkeleton** skeleton;
    spAnimationState* state = nullptr;
    double timeScale = 0.0;
    std::string* skin = nullptr;

	int blink = 255;
    static void animationStateListener(spAnimationState* state, spEventType type, spTrackEntry* entry, spEvent* event);
    static bool finished;
};
