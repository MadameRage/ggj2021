#include "VertexGrabber.hpp"

#include <jngl.hpp>

VertexGrabber::VertexGrabber(const jngl::Vec2 position) : EditorObject(position) {
}

void VertexGrabber::draw() const {
	jngl::setColor(255, isSelected() ? 255 : 0, 0, mouseOver ? 220 : 140);
	jngl::drawEllipse(position.x, position.y, grabRadius, grabRadius);
	if (newVertex) {
		jngl::setColor(0, 255, 0, 140);
		jngl::drawEllipse(newVertex->x, newVertex->y, grabRadius, grabRadius);
	}
}

jngl::Vec2 VertexGrabber::getVertex() const {
	return position;
}
