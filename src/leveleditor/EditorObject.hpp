#pragma once

#include <array>
#include <cereal/cereal.hpp>
#include <jngl/Vec2.hpp>
#include <optional>

class GameObject;

class EditorObject {
public:
	EditorObject() = default;
	EditorObject(jngl::Vec2 position);
	virtual ~EditorObject() = default;

	enum class Action {
		NOTHING,
		CHANGED,
		DELETE_ME,
		NEED_TO_SELECT,
	};

	/// Gibt z. B. CHANGED zurück wenn die Maustaste beim Verschieben losgelassen wird. \a cloneHere
	/// kann auf eine Position gesetzt werden, an der eine Kopie des Objekts erzeugt werden soll.
	virtual Action step(std::optional<jngl::Vec2>& cloneHere, bool multipleSelection,
	                    jngl::Vec2 globalMousePos);

	virtual void draw() const = 0;

	void startDrag(jngl::Vec2 globalMousePos);

	bool isSelected() const;
	virtual void setSelected(bool);

	bool isMouseOver() const;

	void selectIfInside(std::array<jngl::Vec2, 2> rectangle);

	virtual GameObject* spawn() const;

	jngl::Vec2 getPosition() const;
	void setPosition(jngl::Vec2);

protected:
	double grabRadius = 4;
	bool mouseOver = false;
	jngl::Vec2 position;

	// Die Position für eine neue Kopie des EditorObjects
	std::optional<jngl::Vec2> newVertex;

private:
	bool selected = false;
	std::optional<jngl::Vec2> grabStartMousePos;
	std::optional<jngl::Vec2> grabStartVertexPos;
	std::optional<double> grabStartTime;

	friend class cereal::access;
	template <class Archive> void serialize(Archive& ar, const unsigned int) {
		ar(CEREAL_NVP(position));
	}
};
