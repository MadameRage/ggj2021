#include "EditorObject.hpp"

#include "../engine/helper.hpp"

#include <jngl/all.hpp>

EditorObject::EditorObject(const jngl::Vec2 position) : position(position) {
}

EditorObject::Action EditorObject::step(std::optional<jngl::Vec2>& cloneHere,
                                        const bool /*multipleSelection*/,
                                        jngl::Vec2 globalMousePos) {
	mouseOver = boost::qvm::mag_sqr(globalMousePos - position) < grabRadius * grabRadius;
	if (grabStartMousePos) {
		if (!jngl::mouseDown(jngl::mouse::Left)) {
			if (boost::qvm::mag_sqr(*grabStartMousePos - globalMousePos) < 100 &&
			    jngl::getTime() - *grabStartTime < 0.5) {
				// Weniger als 10px bewegt und kürzer als 100ms?
				position = *grabStartVertexPos;
				setSelected(false); // abwählen damit nur noch der mit mouseover ausgewählt wird.
			}
			grabStartMousePos = std::nullopt;
			grabStartVertexPos = std::nullopt;
			grabStartTime = std::nullopt;
			return selected ? Action::CHANGED : Action::NEED_TO_SELECT;
		}
		position = roundToGrid(*grabStartVertexPos + (globalMousePos - *grabStartMousePos));
	}
	if (mouseOver && jngl::mousePressed(jngl::mouse::Middle)) { return Action::DELETE_ME; }
	if (mouseOver && jngl::keyPressed(jngl::key::AltL)) {
		newVertex = position;
	}
	if (newVertex) { newVertex = roundToGrid(globalMousePos); }
	if (newVertex && !jngl::keyDown(jngl::key::AltL)) {
		if (position != *newVertex) { // Don't add two vertices exactly on top of each other
			cloneHere = newVertex;
		}
		newVertex = {};
	}
	return Action::NOTHING;
}

void EditorObject::startDrag(jngl::Vec2 globalMousePos) {
	grabStartMousePos = globalMousePos;
	grabStartVertexPos = position;
	grabStartTime = jngl::getTime();
}

bool EditorObject::isSelected() const {
	return selected;
}

void EditorObject::setSelected(const bool selected) {
	this->selected = selected;
}

bool EditorObject::isMouseOver() const {
	return mouseOver;
}

void EditorObject::selectIfInside(std::array<jngl::Vec2, 2> rectangle) {
	setSelected(position.x > rectangle[0].x && position.y > rectangle[0].y &&
	            position.x < rectangle[1].x && position.y < rectangle[1].y);
}

GameObject* EditorObject::spawn() const {
	return nullptr;
}

jngl::Vec2 EditorObject::getPosition() const {
	return position;
}

void EditorObject::setPosition(const jngl::Vec2 position) {
	this->position = position;
}
