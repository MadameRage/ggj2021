#pragma once

#include "EditorObject.hpp"

#include <jngl/Vec2.hpp>

class VertexGrabber : public EditorObject {
public:
	VertexGrabber() = default; // für cereal
	explicit VertexGrabber(jngl::Vec2 position);

	void draw() const override;

	jngl::Vec2 getVertex() const;
};
