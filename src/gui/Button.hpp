#pragma once

#include <jngl.hpp>

class Button : public jngl::Widget {
public:
	Button(const std::string& label, jngl::Vec2 position, std::function<void()> onClicked);

	Action step() override;
	void drawSelf() const override;

private:
	static jngl::Font& font();

	std::function<void()> onClicked;
	jngl::TextLine label;
	bool mouseover = false;
};
