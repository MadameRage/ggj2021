#include "Button.hpp"

Button::Button(const std::string& label, jngl::Vec2 position, std::function<void()> onClicked)
: jngl::Widget(position), onClicked(std::move(onClicked)), label(font(), label) {
}

Button::Action Button::step() {
	const auto mouse = jngl::getMousePos() + label.getSize() / 2.;
	if ((mouseover = (position.x <= mouse.x && mouse.x < position.x + label.getWidth() &&
	                  position.y <= mouse.y && mouse.y < position.y + label.getHeight()))) {
		if (jngl::mousePressed()) {
			jngl::setMousePressed(jngl::mouse::Left, false);
			onClicked();
		}
	}
	return Widget::step();
}

void Button::drawSelf() const {
	jngl::setFontColor(mouseover ? 0x555555_rgb : 0x000000_rgb);
	label.draw();
}

jngl::Font& Button::font() {
	static jngl::Font f{ "victor-pixel.ttf", 8 };
	return f;
}
