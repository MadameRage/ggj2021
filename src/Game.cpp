#include "Game.hpp"

#include "constants.hpp"
#include "engine/helper.hpp"
#include "globals.hpp"
#include "Level.hpp"
#include "LevelEditor.hpp"
#include "Player.hpp"
#include "WinningScreen.hpp"

#include <cmath>
#include <map>

using jngl::Vec2;

Game::Game() : world({ 0, 40 /* gravity */ }) {
	// hier und nicht in der initlist, da der ctor von Level Funktionen von Game (insbesondere
	// addTriangle) aufruft und dafür Game schon initialisiert sein muss:
	level = std::make_unique<Level>(*this);

	world.SetContactListener(&contactListener);

	gameObjects.emplace_back(std::make_shared<Player>(world, jngl::Vec2(0, 0), 0));
	gameObjects.emplace_back(std::make_shared<Player>(world, jngl::Vec2(100, 0), 1));
}

Game::~Game() {
	gameObjects.clear();
}

void Game::step() {
	world.Step(1.f / 60.f, 8, 3);

	stepCamera(true);

	playersAliveCount = 0;
	for (auto it = gameObjects.begin(); it != gameObjects.end(); ++it) {
		if ((*it)->step()) {
			it = gameObjects.erase(it);
			if (it == gameObjects.end()) {
				break;
			}
		} else if (const auto player = dynamic_cast<Player*>(it->get())) {
			if (player->isAlive()) {
				++playersAliveCount;
				possibleWinner = player;
			}
		}
	}
	for (auto it = animations.begin(); it != animations.end(); ++it) {
		if ((*it)->step()) {
			it = animations.erase(it);
			if (it == animations.end()) {
				break;
			}
		}
	}

#ifndef NDEBUG
	if (jngl::keyPressed(jngl::key::F1)) {
		g_showBoundingBoxes = !g_showBoundingBoxes;
	}
	if (jngl::keyPressed(jngl::key::F3)) {
		jngl::setWork(std::make_shared<LevelEditor>(shared_from_this(), *level));
	}
	if (jngl::keyPressed(jngl::key::F5)) {
		jngl::setWork(std::make_shared<Game>());
	}
#endif
}

void Game::draw() const {
	jngl::pushMatrix();
	applyCamera();
	jngl::setColor(30, 200, 30, 255);
	for (const auto& triangle : triangles) {
		triangle.draw();
	}

	std::multimap<double, GameObject*> orderedByZIndex;
	std::vector<const Player*> players;
	for (const auto& obj : gameObjects) {
		orderedByZIndex.emplace(obj->getZIndex(), obj.get());
		if (const auto player = dynamic_cast<Player*>(obj.get())) {
			players.emplace_back(player);
		}
	}

	for (const auto& gameObject : orderedByZIndex) {
		gameObject.second->draw();
	}

	for (const auto& animation : animations) {
		animation->draw();
	}
	jngl::popMatrix();
}

int Game::playersAlive() const {
	return playersAliveCount;
}

std::vector<std::array<Vec2, 3>> Game::addTriangle(const Vec2& a, const Vec2& b, const Vec2& c,
                                                   AreaColor color) {
	const Vec2 position = (a + b + c) / 3.;
	assert(pointInsideTriangle(position, { a, b, c }));
	auto data = std::array<jngl::Vec2, 3>{ a, b, c };
	triangles.emplace_back(world, data, color);
	return { data };
}

jngl::Vec2 Game::getAbsoluteMousePos() const {
	return jngl::getMousePos() / cameraZoom + cameraPosition;
}

void Game::applyCamera() const {
	jngl::scale(cameraZoom);
	jngl::translate(-1 * cameraPosition);
}

double Game::getCameraZoom() const {
	return cameraZoom;
}

Vec2 Game::getCameraSpeed() const {
	return targetCameraPosition - cameraPosition;
}

Vec2 Game::getCameraPosition() const {
	return cameraPosition;
}

template <typename T> int sgn(T val) {
	return (T(0) < val) - (val < T(0));
}

void Game::setCameraPosition(Vec2 position, const double deadzoneFactorX,
                             const double deadzoneFactorY) {
	cameraDeadzone = position - targetCameraPosition;
	const double x = 160 * deadzoneFactorX;
	const double y = 90 * deadzoneFactorY;
	if (std::abs(cameraDeadzone.x) > x) {
		cameraDeadzone.x =
		    sgn(cameraDeadzone.x) *
		    (x + (std::abs(cameraDeadzone.x) - x) / std::exp(std::abs(cameraDeadzone.x) - x));
	}
	if (std::abs(cameraDeadzone.y) > y) {
		cameraDeadzone.y =
		    sgn(cameraDeadzone.y) *
		    (y + (std::abs(cameraDeadzone.y) - y) / std::exp(std::abs(cameraDeadzone.y) - y));
	}
	// cameraDeadzone.x = std::clamp(cameraDeadzone.x, -160. * deadzoneFactor, 160. *
	// deadzoneFactor); cameraDeadzone.y = std::clamp(cameraDeadzone.y, -90. * deadzoneFactor, 90. *
	// deadzoneFactor);
	targetCameraPosition = position - cameraDeadzone;
}

void Game::setCameraPositionImmediately(Vec2 position) {
	targetCameraPosition = cameraPosition = position;
	speedAverage = 0;
}

void Game::stepCamera(bool mouseWheelZoom) {
	const auto speed = getCameraSpeed();
	const double NUMBER_OF_FRAMES = 500;
	speedAverage *= (NUMBER_OF_FRAMES - 1) / NUMBER_OF_FRAMES;
	speedAverage += // Wir berücksichtigen die y-Kombonente stärker, aufgrund des Breitbildformats
	    boost::qvm::mag_sqr(jngl::Vec2(0.9 * speed.x, 1.6 * speed.y)) / NUMBER_OF_FRAMES;
	cameraPosition += speed / 36.0;
	if (mouseWheelZoom) {
		targetCameraExponent += jngl::getMouseWheel() * 0.1;
	} else {
		// Weniger als -log(4) darf der Zoom nicht sein, da man sonst den Rand des Backgrounds sieht
		const double FACTOR = 600.;
		speedAverage = std::min(std::pow(std::log(4) * FACTOR, 2), speedAverage);
		targetCameraExponent = -std::sqrt(speedAverage) / FACTOR;
	}
	cameraExponent += (targetCameraExponent - cameraExponent) / 17.0;
	const double newCameraZoom = std::exp(cameraExponent);
	if (mouseWheelZoom) {
		// Die Kamera-Position so berechnen, dass sich die Weltkoordinaten des Mauszeigers nicht
		// ändern: m / z + c = m / z' + c'
		cameraPosition = getAbsoluteMousePos() - jngl::getMousePos() / newCameraZoom;
	}
	cameraZoom = newCameraZoom;
}

void Game::triangulateBorder() {
	triangles.clear();
	level->triangulateBorder();
}

void Game::onLoad() {
	// Wird z. B. aufgerufen, wenn wir vom WinningScreen wieder aktiv gesetzt wurden.
	playerWon = nullptr;
}
