#include "constants.hpp"
#include "Intro.hpp"

#include <ctime>
#include <jngl/main.hpp>
#include <jngl/message.hpp>
#include <jngl/input.hpp>
#include <jngl/job.hpp>
#include <jngl/init.hpp>

class QuitWithEscape : public jngl::Job {
public:
	void step() override {
		if (jngl::keyPressed(jngl::key::Escape)) {
			jngl::quit();
		}
	}
	void draw() const override {
	}
};

std::function<std::shared_ptr<jngl::Work>()> jnglInit(jngl::AppParameters& params) {
	std::srand(std::time(0));
	params.displayName = programDisplayName;
	params.screenSize = { 1920, 1080 };
	return []() {
		jngl::addJob(std::make_shared<QuitWithEscape>());
		return std::make_shared<Intro>();
	};
}
