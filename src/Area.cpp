#include "Area.hpp"
#include "constants.hpp"

#include <box2d/box2d.h>

Area::Area(b2World& world, const jngl::Vec2 position)
{
	b2BodyDef bodyDef;
	bodyDef.position = pixelToMeter(position);
	bodyDef.type = b2_kinematicBody;
	body = world.CreateBody(&bodyDef);
	body->SetGravityScale(0);

	b2CircleShape shape = b2CircleShape();
	shape.m_radius = 20 / PIXEL_PER_METER;
	createFixtureFromShape(shape);
	body->GetFixtureList()[0].SetFriction(11.7f);
}

Area::~Area(){
	if (body) {
		body->GetWorld()->DestroyBody(body);
	}
}

double Area::getZIndex() const {
	return -9999;
}

bool Area::step() {


	return false;
}

void Area::draw() const {
	jngl::pushMatrix();
	jngl::translate(getPosition());
	sprite.draw();
	jngl::popMatrix();

}
