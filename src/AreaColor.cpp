#include "AreaColor.hpp"

jngl::Color toColor(AreaColor c) {
	switch (c) {
	case AreaColor::RED:
		return 0xff2222_rgb;
		break;
	case AreaColor::BLUE:
		return 0x2222ff_rgb;
		break;
	case AreaColor::PURPLE:
		return 0xbb22ff_rgb;
		break;
	}
	return 0xbc457f_rgb;
}

uint16_t toFilter(AreaColor color) {
	switch (color) {
	case AreaColor::RED:
		return 0x01;
		break;
	case AreaColor::BLUE:
		return 0x02;
		break;
	case AreaColor::PURPLE:
		return 0;
	}
	return 0;
}
