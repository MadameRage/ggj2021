#pragma once

#include "GameObject.hpp"

#include <jngl.hpp>

class b2World;

class Area : public GameObject
{
public:
    Area(b2World &world, const jngl::Vec2 position);
    ~Area();

    bool step() override;

    void draw() const override;

	double getZIndex() const override;

private:
	jngl::Sprite sprite{"button_a"};

};
