#include "DebugDraw.hpp"

#include "globals.hpp"

void DrawShape(jngl::Vec2 pos, b2Shape* shape) {
	if (!g_showBoundingBoxes) return;
	if (const auto circle = dynamic_cast<b2CircleShape*>(shape)) {
		jngl::setColor(255, 0, 0, 150);
		jngl::drawCircle(pos, circle->m_radius * PIXEL_PER_METER);
	}
}

void DrawShape(b2Body* body) {
	if (!g_showBoundingBoxes) return;
	b2Fixture* fixture = body->GetFixtureList();
	DrawShape(meterToPixel(body->GetPosition()), fixture->GetShape());
}
