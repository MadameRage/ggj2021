#include "Player.hpp"

#include "AreaColor.hpp"
#include "DebugDraw.hpp"
#include "Gamepad.hpp"
#include "Keyboard.hpp"

#include <cmath>

Player::Player(b2World& world, const jngl::Vec2 position, const int playerNr)
: playerNr(playerNr), color(playerNr == 0 ? AreaColor::RED : AreaColor::BLUE) {
	b2BodyDef bodyDef;
	bodyDef.position = pixelToMeter(position);
	bodyDef.type = b2_dynamicBody;
	body = world.CreateBody(&bodyDef);
	body->GetUserData().pointer = reinterpret_cast<uintptr_t>(static_cast<GameObject*>(this));
	body->SetLinearDamping(10.f);

	b2CircleShape shape = b2CircleShape();
	shape.m_radius = 6 / PIXEL_PER_METER;
	createFixtureFromShape(shape);

	const auto controllers = jngl::getConnectedControllers();
	if (controllers.size() > playerNr) {
		control = std::make_unique<Gamepad>(controllers[playerNr], playerNr);
	} else {
		control = std::make_unique<Keyboard>(playerNr);
	}
}

Player::~Player() {
	if (body) {
		body->GetWorld()->DestroyBody(body);
	}
}

bool Player::step() {
	if (!alive) {
		return false;
	}

	if (control->jump()) {
		if (remainingJumpSteps <= 0 && jumpAvailable) {
			remainingJumpSteps = 6;
			jumpAvailable = false;
		}
	} else {
		--remainingJumpSteps; // wenn man Springen länger hält, soll man höher springen können
		jumpAvailable = true;
	}
	if (remainingJumpSteps > 0) {
		float force = body->GetMass() * 100;
		body->ApplyForce(b2Vec2(0, -force), body->GetWorldCenter(), true);
		--remainingJumpSteps;
	}
	if (control->jetpack()) {
		body->ApplyForce(b2Vec2(0, body->GetMass() * -70), body->GetWorldCenter(), true);
	}
	body->ApplyForce(pixelToMeter(20 * control->getMovement()), body->GetWorldCenter(), true);

	time += float(90 + (rand() % 10)) / 1000.f;
	return false;
}

void Player::draw() const {
	jngl::pushMatrix();
	jngl::translate(getPosition());
	jngl::setColor(toColor(color));
	jngl::rotate(getRotation() * 180 / M_PI);
	const double SIZE = 13;
	jngl::drawRect({ -SIZE / 2., -SIZE / 2. }, { SIZE, SIZE });
	jngl::popMatrix();

	DrawShape(body); // DEBUG
}

void Player::onContact(GameObject* other) {
	if (!alive) {
		return;
	}
}

bool Player::isRepaired() const {
	return true;
}

bool Player::isAlive() const {
	return alive;
}

bool Player::isKing() const {
	return king;
}

void Player::setKing(bool king) {
	this->king = king;
}

void Player::vibrate() {
	control->vibrate();
}

void Player::createFixtureFromShape(const b2Shape& shape) {
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = 1.0f;
	fixtureDef.friction = 0.7f;
	fixtureDef.restitution = 0.1f;
	fixtureDef.filter.categoryBits = toFilter(color);
	fixtureDef.filter.maskBits = 0xffff;
	body->CreateFixture(&fixtureDef);
	body->SetGravityScale(1);
}
