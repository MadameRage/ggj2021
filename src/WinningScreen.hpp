#pragma once

#include "spine/AnimationState.h"

#include <jngl/work.hpp>
#include <string>

struct spSkeleton;
struct spAnimationState;
struct spEvent;

namespace spine {
class SkeletonDrawable;
}

class WinningScreen : public jngl::Work {
public:
	WinningScreen(std::shared_ptr<jngl::Work> game);
	~WinningScreen();
	void step() override;
	void draw() const override;

private:
	std::unique_ptr<spine::SkeletonDrawable> skeleton;
    spSkeletonData* skeletonData = nullptr;

    // spSkeleton** skeleton;
    spAnimationState* state = nullptr;
    double timeScale = 0.0;
    std::string* skin = nullptr;

	int blink = 255;
    static void animationStateListener(spAnimationState* state, spEventType type, spTrackEntry* entry, spEvent* event);
    static int finished_count;

    std::shared_ptr<jngl::Work> game;

    /// Zählt hoch
    int time = 0;
};
