#include "BorderArea.hpp"

#include "engine/helper.hpp"

using jngl::Vec2;

BorderArea::BorderArea(std::vector<Vec2> vertices, std::vector<std::vector<Vec2>> holes) {
	for (auto& vertex : vertices) {
		this->vertices.emplace_back(vertex);
	}
	for (auto& hole : holes) {
		std::vector<VertexGrabber> newHole;
		for (auto& vertex : hole) {
			newHole.emplace_back(vertex);
		}
		this->holes.emplace_back(std::move(newHole));
	}
}

void BorderArea::addHole(Vec2 pos) {
	holes.emplace_back(std::vector<VertexGrabber>{ VertexGrabber(jngl::Vec2(pos.x, pos.y)) });
}

bool BorderArea::contains(Vec2 pos) const {
	for (const auto& triangle : triangles) {
		if (pointInsideTriangle(pos, triangle)) {
			return true;
		}
	}
	return false;
}

bool BorderArea::step(const bool multipleSelection, jngl::Vec2 globalMousePos) {
	bool changed = false;
	for (auto it = vertices.begin(); it != vertices.end(); ++it) {
		std::optional<Vec2> newVertex;
		VertexGrabber::Action result = it->step(newVertex, multipleSelection, globalMousePos);
		if (result != VertexGrabber::Action::NOTHING) {
			changed = true;
		}
		if (result == VertexGrabber::Action::DELETE_ME) {
			vertices.erase(it);
			break;
		}
		if (newVertex) {
			const auto previousIt = (it == vertices.begin()) ? (vertices.end() - 1) : (it - 1);
			const auto nextIt = (it + 1 == vertices.end()) ? vertices.begin() : (it + 1);
			// Soll der neue Vertex *vor* oder *nach* dem aktuellen hinzugefügt werden?
			if (boost::qvm::mag_sqr(previousIt->getVertex() - *newVertex) >
			    boost::qvm::mag_sqr(nextIt->getVertex() - *newVertex)) {
				vertices.insert(nextIt, VertexGrabber(jngl::Vec2(*newVertex))); // vor dem aktuellen
			} else {
				vertices.insert(it, VertexGrabber(jngl::Vec2(*newVertex))); // nach dem aktuellen
			}
			changed = true;
			break;
		}
	}
	for (auto& hole : holes) {
		for (auto it = hole.begin(); it != hole.end(); ++it) {
			std::optional<Vec2> newVertex;
			VertexGrabber::Action result = it->step(newVertex, multipleSelection, globalMousePos);
			if (result != VertexGrabber::Action::NOTHING && hole.size() > 2) {
				changed = true;
			}
			if (result == VertexGrabber::Action::DELETE_ME) {
				hole.erase(it);
				break;
			}
			if (newVertex) {
				const auto previousIt = (it == hole.begin()) ? (hole.end() - 1) : (it - 1);
				const auto nextIt = (it + 1 == hole.end()) ? hole.begin() : (it + 1);
				// Soll der neue Vertex *vor* oder *nach* dem aktuellen hinzugefügt werden?
				if (boost::qvm::mag_sqr(previousIt->getVertex() - *newVertex) >
				    boost::qvm::mag_sqr(nextIt->getVertex() - *newVertex)) {
					hole.insert(nextIt, VertexGrabber(*newVertex)); // vor dem aktuellen
				} else {
					hole.insert(it, VertexGrabber(*newVertex)); // nach dem aktuellen
				}
				if (hole.size() > 2) {
					changed = true;
				}
				break;
			}
		}
	}
	if (selected) {
		if (jngl::keyPressed('f')) {
			if (color == AreaColor::RED) {
				color = AreaColor::BLUE;
			} else if (color == AreaColor::BLUE) {
				color = AreaColor::PURPLE;
			} else {
				color = AreaColor::RED;
			}
			changed = true;
		}
	}
	return changed;
}

void BorderArea::setSelected(bool s) {
	selected = s;
}

void BorderArea::draw() const {
	jngl::Vec2 topLeft{ 9999, 9999 };
	for (auto& vertexGrabber : vertices) {
		vertexGrabber.draw();
		if (vertexGrabber.getPosition().x < topLeft.x ||
		    vertexGrabber.getPosition().y < topLeft.y) {
			topLeft = vertexGrabber.getPosition();
		}
	}
	if (selected) {
		jngl::print("[F] = Farbe ändern", topLeft);
	}
	for (auto& hole : holes) {
		for (auto& vertexGrabber : hole) {
			vertexGrabber.draw();
		}
	}
}

bool BorderArea::mouseOverVertexGrabber() const {
	for (auto& vertexGrabber : vertices) {
		if (vertexGrabber.isMouseOver()) {
			return true;
		}
	}
	for (auto& hole : holes) {
		for (auto& vertexGrabber : hole) {
			if (vertexGrabber.isMouseOver()) {
				return true;
			}
		}
	}
	return false;
}

void BorderArea::create(
    const std::function<std::vector<std::array<Vec2, 3>>(
        std::vector<Vec2> vertices, std::vector<std::vector<Vec2>> holes, AreaColor)>& callback) {

	std::vector<Vec2> plainVertices;
	for (const auto& vertexGrabber : vertices) {
		plainVertices.push_back(vertexGrabber.getVertex());
	}
	std::vector<std::vector<Vec2>> plainHoles;
	for (auto& hole : holes) {
		plainHoles.emplace_back();
		for (const auto& vertexGrabber : hole) {
			plainHoles.back().push_back(vertexGrabber.getVertex());
		}
	}
	triangles = callback(plainVertices, plainHoles, color);
}

std::vector<EditorObject*> BorderArea::getEditorObjects() {
	std::vector<EditorObject*> editorObjects;
	for (auto& vertexGrabber : vertices) {
		editorObjects.emplace_back(&vertexGrabber);
	}
	for (auto& hole : holes) {
		for (auto& vertexGrabber : hole) {
			editorObjects.emplace_back(&vertexGrabber);
		}
	}
	return editorObjects;
}
