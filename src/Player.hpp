#pragma once

#include "AreaColor.hpp"
#include "GameObject.hpp"
#include "engine/Animation.hpp"

#include <array>
#include <box2d/box2d.h>
#include <jngl.hpp>
#include <memory>

class b2World;
class Button;
class Control;
class ButtonFragment;

class Player : public GameObject {
public:
	Player(b2World& world, const jngl::Vec2 position, const int playerNr);
	~Player() override;

	Player(const Player&) = delete;
	Player& operator=(const Player&) = delete;
	Player(Player&&) = delete;
	Player& operator=(Player&&) = delete;

	bool step() override;

	void draw() const override;

	void onContact(GameObject*) override;

	void createFixtureFromShape(const b2Shape& shape);
	bool isRepaired() const;
	bool isAlive() const;

	/// Der Spieler hat alles eingesammelt und kann jetzt töten
	bool isKing() const;
	void setKing(bool);

	void vibrate();

	int shield_up_time = 0;
	int points = 0;

private:
	bool king = false;

	const int playerNr;

	// wird ungleichmäßig hochgezählt für die Animation des Kopfs
	float time = 0;

	int dashCountdown = 0;
	b2Vec2 dashDirection;

	int remainingJumpSteps = 0;
	bool jumpAvailable = true;

	std::unique_ptr<Control> control;

	bool alive = true;

	AreaColor color;
};
